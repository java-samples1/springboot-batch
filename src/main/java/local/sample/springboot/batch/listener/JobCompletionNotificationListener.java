package local.sample.springboot.batch.listener;

import org.springframework.batch.core.BatchStatus;
import org.springframework.batch.core.JobExecution;
import org.springframework.batch.core.listener.JobExecutionListenerSupport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

import local.sample.springboot.batch.domain.model.Person;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
public class JobCompletionNotificationListener extends JobExecutionListenerSupport {

	private final JdbcTemplate jdbcTemplate;

	@Autowired
	public JobCompletionNotificationListener(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	@Override
	public void afterJob(JobExecution jobExecution) {

		if (jobExecution.getStatus() == BatchStatus.COMPLETED) {
			log.info("JOB FINISHED!! ");
		}

		jdbcTemplate
				.query("SELECT first_name, last_name FROM people",
						(rs, row) -> new Person(rs.getString(1), rs.getString(2)))
				.forEach(person -> log.info("Found <{}> in the database.", person));
	}

	@Override
	public void beforeJob(JobExecution jobExecution) {
		log.info("JOB START!!");
	}

}
