package local.sample.springboot.batch.config.job;

import org.springframework.batch.core.Job;
import org.springframework.batch.core.Step;
import org.springframework.batch.core.configuration.annotation.EnableBatchProcessing;
import org.springframework.batch.core.configuration.annotation.JobBuilderFactory;
import org.springframework.batch.core.configuration.annotation.StepBuilderFactory;
import org.springframework.batch.core.launch.support.RunIdIncrementer;
import org.springframework.batch.item.database.JdbcBatchItemWriter;
import org.springframework.batch.item.file.FlatFileItemReader;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import local.sample.springboot.batch.domain.model.Person;
import local.sample.springboot.batch.domain.processer.PersonItemProcessor;
import local.sample.springboot.batch.listener.JobCompletionNotificationListener;

@Configuration
@EnableBatchProcessing
public class JobConfig {

	@Autowired
	private JobBuilderFactory jobBuilder;

	@Autowired
	private StepBuilderFactory stepBuilder;

	@Bean
	public Job importUserJob(JobCompletionNotificationListener listener, Step step1) {
		return jobBuilder.get("importUserJob").incrementer(new RunIdIncrementer()).listener(listener).flow(step1).end()
				.build();
	}

	@Bean
	public Step step1(FlatFileItemReader<Person> reader, PersonItemProcessor processor,
			JdbcBatchItemWriter<Person> writer) {
		return stepBuilder.get("step1").<Person, Person>chunk(10).reader(reader).processor(processor).writer(writer)
				.build();

	}
}
