package local.sample.springboot.batch.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import local.sample.springboot.batch.domain.processer.PersonItemProcessor;

@Configuration
public class ProcessorConfig {

	@Bean
	public PersonItemProcessor processor() {
		return new PersonItemProcessor();
	}

}
