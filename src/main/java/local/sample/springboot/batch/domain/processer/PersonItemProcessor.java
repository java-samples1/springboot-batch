package local.sample.springboot.batch.domain.processer;

import org.springframework.batch.item.ItemProcessor;

import local.sample.springboot.batch.domain.model.Person;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class PersonItemProcessor implements ItemProcessor<Person, Person> {

	@Override
	public Person process(final Person person) throws Exception {

		final String firstName = person.getFirstName().toUpperCase();
		final String lastName = person.getLastName().toUpperCase();

		final Person transformedPerson = new Person(firstName, lastName);
		log.info("Converting, ({}) into ({})", person, transformedPerson);

		return transformedPerson;
	}
}
